# Copyright (C) 2016-2020 Davidson Francis <davidsondfgl@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

CC      ?= gcc
WSDIR    = $(CURDIR)/src
INCLUDE  = -I $(WSDIR)
CFLAGS   =  -Wall -Wextra -O2 -lcjson -lpigpio -DLOG_USE_COLOR
CFLAGS  +=  $(INCLUDE) -std=c99 -pthread -pedantic -lm
LIB      =  $(WSDIR)/../build/libws.a
SYTEMDSYSDIR = $(SYSCONFDIR)/systemd/system

# PREFIX is environment variable, but if it is not set, then set default value
ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

# Examples
all: raspicar

deps: libws.a 
	sudo apt update
	sudo apt install -y libcjson-dev libi2c-dev linux-libc-dev

libws.a:
	mkdir -p build
	rm -rf wsserver
	git clone https://gitlab.com/ttblt-hass/wsserver.git
	cd wsserver && make
	cp wsserver/libws.a build/libws.a
	cp wsserver/include/ws.h src/ws.h
	rm -rf wsserver

# Send receive
raspicar:
	mkdir -p build/
	$(CC) $(CFLAGS) $(LDFLAGS) src/*.c -o build/raspicar $(LIB)

frontend:
	cd webui && npm run build

# Clean
clean:
	@rm -f build/raspicar

install:
	install -d $(DESTDIR)$(PREFIX)/bin/
	install -m 755 build/raspicar $(DESTDIR)$(PREFIX)/bin/
	#install -m 755 raspicar.system $(DESTDIR)$(PREFIX)/bin/
	install -d /etc/systemd/system
	install -m 644 misc/raspicar.service /etc/systemd/system/
	install -m 755 streamer/raspicar-streamer.py $(DESTDIR)$(PREFIX)/bin/
	install -m 644 misc/raspicar-streamer.service /etc/systemd/system/
