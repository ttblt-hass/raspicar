#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "i2c.h"
#include "log.h"
#include <pigpio.h>

struct I2cSensor *setupI2cSensor(char *name, char *model, int addr) {
  log_info("Sensor creating - %s", name);
  struct I2cSensor *sensor =
      (struct I2cSensor *)malloc(sizeof(struct I2cSensor));
  sensor->name = name;
  sensor->model = model;
  sensor->addr = addr;
  sensor->i2cfh = i2cOpen(1, addr, 0);
  log_debug("Sensor created - %s", name);
  return sensor;
}
