#include <linux/i2c-dev.h>
#include <pigpio.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

// char *bus = "/dev/i2c-1";

#ifndef I2C_H
#define I2C_H

struct I2cSensor {
  char *name;
  char *model;
  int addr;
  int i2cfh;
};

struct I2cSensor *setupI2cSensor(char *name, char *model, int addr);

#endif /* I2C_H */
