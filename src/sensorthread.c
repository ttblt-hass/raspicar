#include <pthread.h>
#include <stdio.h>

#include "ads1115.h"
#include "common.h"
#include "i2c.h"
#include "log.h"
#include "mpu6050.h"
#include "sensorthread.h"
#include "wsserver.h"
#include "rccar.h"

void *sensorThreadFun(void *vargp) {
  log_info("Sensor Thread - starting");
  // detach the current thread
  // from the calling thread
  // pthread_detach(pthread_self());

  struct RCCar *car = (struct RCCar *)(vargp);
  while (nbClientConnected > 0) {
    struct timespec tim, tim2;
    tim.tv_sec = 0;
    tim.tv_nsec = 10000000; // 10ms
    nanosleep(&tim, &tim2);
    for (unsigned int i = 0;
         i < sizeof(car->i2csensors) / sizeof(struct I2cSensor); i++) {
      if (car->i2csensors[i] != NULL) {
        log_trace("Sensor Thread - fetching %s", car->i2csensors[i]->name);
        if (strcmp(car->i2csensors[i]->model, "ads1115") == 0) {
          char *msg = malloc(1024 * sizeof(char));
          ADS1115read(msg, car->i2csensors[i]->name, car->i2csensors[i]->i2cfh,
                      0);
          wssendmessage(car->wsfd, msg);
          free(msg);
        }
        if (strcmp(car->i2csensors[i]->model, "mpu6050") == 0) {
          char *msg = malloc(1024 * sizeof(char));
          MPU6050read(msg, car->i2csensors[i]->name, car->i2csensors[i]->i2cfh);
          wssendmessage(car->wsfd, msg);
          free(msg);
        }
      }
    }
    int latency = millis() - lastPing;
    if (latency > 500) {
      log_error("Latency too high (%d > 500ms). Stopping the car", latency);
      shutdownRCCar(car);
    }
  }
  log_info("Sensor Thread - stopped");
  pthread_exit(0);
}

void startSensorThread(struct RCCar *car) {
  pthread_create(&tid, NULL, sensorThreadFun, (void *)car);
}

void stopSensorThread() {
  log_info("Sensor Thread - stopping");
  nbClientConnected = 0;
  pthread_join(tid, NULL);
}
