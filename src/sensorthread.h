

#ifndef SENSORTHREAD_H
#define SENSORTHREAD_H

#include "rccar.h"
#include <pthread.h>

pthread_t tid;

void *sensorThreadFun(void *vargp);
void startSensorThread(struct RCCar *car);
void stopSensorThread();

#endif /* SENSORTHREAD_H */
