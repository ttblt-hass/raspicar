/*
 * Copyright (C) 2021 Thibault Cohen <thibault.cohen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef MOTOR_H
#define MOTOR_H

#include "common.h"
#include "rccar.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct Motor {
  char *name;
  int pin_forward;
  int pin_backward;
  int pin_pwm;
  int pwm_channel;
  int pwm_freq;
};

struct Motor *setupMotor(char *name, int pin_forward, int pin_backward, int pin_pwm, int pwm_freq);
int getPWMChannel(int pin_pwm);

void setMotorSpeed(struct RCCar *car, const cJSON *data);
void stopMotors(struct RCCar *car);

#endif /* MOTOR_H */
