#include "mpu6050.h"
#include "log.h"
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <pigpio.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

void MPU6050config(int i2cfh) {
  // Setting up Power Management...
  int power_management;
  power_management =
      i2cReadByteData(i2cfh, MPU6050_REGISTER_POWER_MANAGEMENT_1);
  // Set clock source - X-Gyro
  // power_management &= 0b11111000;
  power_management &= 248;
  power_management |= MPU6050_CLOCK_SOURCE_X_GYRO;
  // Disable sleep
  power_management &= ~(1 << MPU6050_BIT_SLEEP_ENABLED);
  // Enable temperature
  power_management &= ~(1 << MPU6050_BIT_TEMPERATURE_DISABLED);
  i2cWriteByteData(i2cfh, MPU6050_REGISTER_POWER_MANAGEMENT_1,
                   power_management);

  // Set scale - 2000DPS
  int gyro_config;
  gyro_config = i2cReadByteData(i2cfh, MPU6050_REGISTER_GYRO_CONFIG);
  // gyro_config &= 0b11100111;
  gyro_config &= 0xE7;
  gyro_config |= MPU6050_SCALE_2000_DPS << 3;
  i2cWriteByteData(i2cfh, MPU6050_REGISTER_GYRO_CONFIG, gyro_config);

  // Set range - 2G
  int accel_config;
  accel_config = i2cReadByteData(i2cfh, MPU6050_REGISTER_ACCEL_CONFIG);
  // accel_config &= 0b11100111;
  accel_config &= 0xE7;
  accel_config |= (MPU6050_ACCEL_RANGE_2G << 3);
  i2cWriteByteData(i2cfh, MPU6050_REGISTER_ACCEL_CONFIG, accel_config);
}

float MPU6050readData(int i2cfh, int reg, float mod) {
  unsigned int raw_data_high;
  unsigned int raw_data_low;
  raw_data_high = i2cReadByteData(i2cfh, reg);
  raw_data_low = i2cReadByteData(i2cfh, reg + 1);
  int16_t rawdata;
  rawdata = raw_data_high << 8 | raw_data_low;
  /*
  // to get signed value from mpu6050
  if (rawdata > 32768){
          rawdata = rawdata - 65536;
  }*/
  return rawdata * mod;
}

void MPU6050read(char *ret, char *name, int i2cfh) {
  log_trace("Sensor MPU6050 %s - reading", name);
  MPU6050config(i2cfh);
  float acc_x;
  float acc_y;
  float acc_z;
  acc_x = MPU6050readData(i2cfh, ACCEL_XOUT_H, MPU6050_ACCEL_SCALE_MODIFIER_2G);
  acc_y = MPU6050readData(i2cfh, ACCEL_YOUT_H, MPU6050_ACCEL_SCALE_MODIFIER_2G);
  acc_z = MPU6050readData(i2cfh, ACCEL_ZOUT_H, MPU6050_ACCEL_SCALE_MODIFIER_2G);
  float gyro_x;
  float gyro_y;
  float gyro_z;
  gyro_x =
      MPU6050readData(i2cfh, GYRO_XOUT_H, MPU6050_GYRO_SCALE_MODIFIER_2000DEG);
  gyro_y =
      MPU6050readData(i2cfh, GYRO_YOUT_H, MPU6050_GYRO_SCALE_MODIFIER_2000DEG);
  gyro_z =
      MPU6050readData(i2cfh, GYRO_ZOUT_H, MPU6050_GYRO_SCALE_MODIFIER_2000DEG);
  // printf("\nX: %0.2f, Y: %0.2f, Z: %0.2f\n", acc_x, acc_y, acc_z);
  // printf("\nX: %0.2f, Y: %0.2f, Z: %0.2f\n", gyro_x, gyro_y, gyro_z);
  sprintf(ret,
          "{\"subject\": \"sensor\", "
          "\"data\": {"
          "\"name\" : \"%s\", "
          "\"acc_x\": %.2f, "
          "\"acc_y\": %.2f, "
          "\"acc_z\": %.2f, "
          "\"gyro_x\": %.2f, "
          "\"gyro_y\": %.2f, "
          "\"gyro_z\": %.2f"
          "}}",
          name, acc_x, acc_y, acc_z, gyro_x, gyro_y, gyro_z);
  log_trace("Sensor MPU6050 %s - acc_x %0.2f, acc_y %0.2f, acc_z %0.2f, gyro_x "
            "%0.2f, gyro_y %0.2f, gyro_z  %0.2f",
            name, acc_x, acc_y, acc_z, gyro_x, gyro_y, gyro_z);
}
