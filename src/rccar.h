/*
 * Copyright (C) 2021 Thibault Cohen <thibault.cohen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef RCCAR_H
#define RCCAR_H

#include "i2c.h"
#include "led.h"
#include "motor.h"
#include "ws.h"
#include <cjson/cJSON.h>
#include <pigpio.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct RCCar {
  char *brand;
  char *model;
  int wsfd;
  const struct Led *stateLed;
  const struct Led *leds[10];
  const struct Motor *motors[2];
  const struct I2cSensor *i2csensors[10];
};

void carConfig(int fd, struct RCCar *car, const cJSON *data);
void setState(struct RCCar *car, char *state);
void shutdownRCCar(struct RCCar *car);

#endif /* RCCAR_H */
