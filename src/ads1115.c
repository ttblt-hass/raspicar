/*
 * ads1115_rpi.c
 *
 *  Created on: 14 de fev de 2019
 *      Author: giobauermeister
 */

#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <pigpio.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "ads1115.h"
#include "i2c.h"
#include "log.h"

// int deviceAddr = 0x48;
// unsigned char ADS1115writeBuf[3] = {0};

void ADS1115config(int i2cfh, int channel) {
  unsigned int config = 0;
  unsigned char ADS1115writeBuf[3] = {0};

  //config = ADS1115_CONFIG_REG_OS_SINGLE | ADS1115_CONFIG_REG_PGA_4_096V |
  config = ADS1115_CONFIG_REG_OS_SINGLE | ADS1115_CONFIG_REG_PGA_6_144V |
           ADS1115_CONFIG_REG_MODE_SINGLE | ADS1115_CONFIG_REG_DR_128SPS |
           ADS1115_CONFIG_REG_CMODE_TRAD | ADS1115_CONFIG_REG_CPOL_ACTIV_LOW |
           ADS1115_CONFIG_REG_CLATCH_NONLATCH | ADS1115_CONFIG_REG_CQUE_NONE;

  switch (channel) {
  case 0:
    config |= ADS1115_CONFIG_REG_MUX_CHAN_0;
    break;
  case 1:
    config |= ADS1115_CONFIG_REG_MUX_CHAN_1;
    break;
  case 2:
    config |= ADS1115_CONFIG_REG_MUX_CHAN_2;
    break;
  case 3:
    config |= ADS1115_CONFIG_REG_MUX_CHAN_3;
    break;
  default:
    printf("Give a channel between 0-3\n");
  }
  ADS1115writeBuf[0] = 0x01;
  ADS1115writeBuf[1] = config >> 8;
  ADS1115writeBuf[2] = config & 0xFF;
  i2cWriteDevice(i2cfh, (char *)ADS1115writeBuf, 3);
  // usleep(25);
}

void ADS1115read(char *ret, char *name, int i2cfh, int channel) {
  log_trace("Sensor ADS1115 %s - reading", name);
  unsigned char ADS1115writeBuf[3] = {0};
  char readBuf[2] = {0};
  unsigned int analogVal;
  float voltage;

  ADS1115config(i2cfh, channel);
  struct timespec tim, tim2;
  tim.tv_sec = 0;
  tim.tv_nsec = 135000000;
  nanosleep(&tim, &tim2);

  ADS1115writeBuf[0] = 0x00;
  i2cWriteDevice(i2cfh, (char *)ADS1115writeBuf, 1);

  if (i2cReadDevice(i2cfh, readBuf, 2) != 2) // read data and check error
  {
    log_error("Sensor ADS1115 %s - can not read voltage %s", name);
    sprintf(ret,
            "{\"subject\": \"sensor\", \"data\": {\"name\" : \"%s\", "
            "\"value\": null}}",
            name);
  } else {
    analogVal = readBuf[0] << 8 | readBuf[1];
    // https://github.com/esphome/esphome/blob/dev/esphome/components/ads1115/ads1115.cpp#L132
    voltage = (float)analogVal * 0.000187500f;
    sprintf(ret,
            "{\"subject\": \"sensor\", \"data\": {\"name\" : \"%s\", "
            "\"value\": %.2f}}",
            name, voltage);
    log_trace("Sensor ADS1115 %s - voltage %.2f", name, voltage);
  }
}
