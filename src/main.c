/*
 * Copyright (C) 2021 Thibault Cohen <thibault.cohen@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "common.h"
#include "led.h"
#include "log.h"
#include "motor.h"
#include "rccar.h"
#include "ws.h"
#include "wsserver.h"
#include <cjson/cJSON.h>
#include <pigpio.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/**
 * @brief Main routine.
 *
 * @note After invoking @ref ws_socket, this routine never returns,
 * unless if invoked from a different thread.
 */
int main(void) {
  log_info("Starting RaspiCar");
  car = (struct RCCar *)malloc(sizeof(struct RCCar));
  lastPing = 0;
  nbClientConnected = 0;
  gpioCfgSocketPort(7771);
  gpioCfgClock(5, 1, 0);
  if (gpioInitialise() < 0)
    return 1;
  log_info("GPIO ready");
  //Stop all gpio
  stopAllGpio();

  // Setup stateled
  struct Led stateLed = setupLed("status", 17, 22, 27);
  car->stateLed = &stateLed;
  setState(car, "started");
  log_info("Status Led ready");

  signal(SIGINT, handle_sigint);
  signal(SIGKILL, handle_sigkill);
  signal(SIGCONT, handle_sigcont);
  signal(SIGTERM, handle_sigterm);
  //signal(SIGSEGV, handle_sigsegv);
  signal(SIGWINCH, handle_sigwinch);
  // Start WebSocket Server
  struct ws_events evs;
  evs.onopen = &wsonopen;
  evs.onclose = &wsonclose;
  evs.onmessage = &wsonmessage;
  log_info("Starting websocket server");
  ws_socket(&evs, 7770); /* Never returns. */

  return (0);
}
